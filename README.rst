Shortest Paths
##############


An exploration of shortest paths in random grids
++++++++++++++++++++++++++++++++++++++++++++++++


|license| |release| |pipeline| |coverage| |status|

.. |license| image:: https://img.shields.io/badge/dynamic/json.svg?label=license&url=https://gitlab.com/api/v4/projects/19084927?license=True&query=$.license.key&colorB=blue
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/tree/trunk/LICENSE

.. |release| image:: https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://gitlab.com/api/v4/projects/19084927/releases&query=$[0].tag_name&colorB=blue
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/releases

.. |pipeline| image:: https://gitlab.com/physics-complex-systems/sociophysics/badges/trunk/pipeline.svg
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/pipelines

.. |coverage| image:: https://gitlab.com/physics-complex-systems/sociophysics/badges/trunk/coverage.svg
   :target: https://gitlab.com/physics-complex-systems/sociophysics/-/pipelines

.. |status| image:: https://www.repostatus.org/badges/latest/wip.svg
   :alt: Project status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.
   :target: https://www.repostatus.org/#wip


An exploration of shortest paths in random grids

.. inclusion-marker-do-not-remove

Description
===========

Context [galam]_, background, purpose, use, features. Add visuals.

----

Setup
=====

Requirements
------------

Prerequisites, dependencies, etc.


Install
-------


----

Usage
=====

Include :ref:`examples <examples>` , e.g. `examples`_

.. _examples: https://physics-complex-systems.gitlab.io/sociophysics/examples.html

.. code-block:: python

   import sociophysics

   sociophysics.doThis() # does This
   sociophysics.returnThat() # returns That

----

Contact
=======

To request support, report an issue, or contribute, please `📩 contact`__ the author.

.. __: mailto:miguelgdonoso@gmail.com

----

License
=======

This project is licensed under the `📜 BSD-2-Clause License`__. See the `LICENSE file`__ for details.

.. __: https://opensource.org/licenses/BSD-2-Clause
.. __: https://github.com/MiguelGDonoso/shortest-paths/blob/master/LICENSE

----

Sources 
=======

Acknowledgements
----------------

* `Semantic Versioning`_
* `Keep a Changelog`_

.. _`Semantic Versioning`: https://semver.org/spec/v2.0.0.html
.. _`Keep a Changelog`: https://keepachangelog.com/en/1.0.0/

Contributors 
------------

* Author - Miguel Gómez Donoso - `MiguelGDonoso`_ - miguelgdonoso@gmail.com

.. _`MiguelGDonoso`: https://gitlab.com/mgdonoso

References
----------

.. [galam] Galam, Sociophysics

