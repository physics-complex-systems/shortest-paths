#include "timer.hpp"

namespace timer {

std::string iso_time_format(double seconds) {
    auto              s = static_cast<int>(seconds);
    std::stringstream ss;
    ss.fill('0');
    ss << std::setw(2) << s / 3600 << ":" << std::setw(2) << (s / 60) % 60 << ":" << std::setw(2)
       << s % 60;
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, Timer const& timer) {
    os << "elapsed: " << iso_time_format(timer.elapsed());
    return os;
}

}  // namespace timer