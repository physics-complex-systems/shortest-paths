#include "distributions.hpp"

namespace distributions {

std::string name(ParetoDistribution) { return "pareto"; }

std::string params(ParetoDistribution dist, int precision) {
    std::stringstream ss;
    ss << std::setprecision(precision);
    ss << "(scale:" << dist.scale << ",shape:" << dist.shape << ")";
    return ss.str();
}

double mean(ParetoDistribution dist) {
    double m = dist.shape * dist.scale / (dist.shape - 1);
    return m >= 0 ? m : std::numeric_limits<double>::infinity();
}

double var(ParetoDistribution dist) {
    double v = dist.shape * pow(dist.scale, 2) / (pow(dist.shape - 1, 2) * (dist.shape - 2));
    return v >= 0 ? v : std::numeric_limits<double>::infinity();
}

}  // namespace distributions