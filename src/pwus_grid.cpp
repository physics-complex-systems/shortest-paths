#include "pwus_grid.hpp"

namespace pwus_grid {

// The path is returned from leaf to root
Path root_path(Tree const& t, Node const leaf) {
    Nodes nodes;
    nodes.reserve(t.parents.size());
    Node const root_parent = -1;
    for (Node n = leaf; n != root_parent; n = t.parents[n]) {
        nodes.push_back(n);
    }
    nodes.shrink_to_fit();
    return {t.costs[leaf], nodes};
}

// Convert a trie to an array of paths.
Paths to_paths(Trie const& t) {
    Paths paths;
    paths.reserve(t.costs.size());
    for (auto const& item : t.costs) {
        NodeIdx spur_idx = 0;
        auto    path_idx = item.second;
        Nodes   path;
        path.reserve(t.branches[path_idx].size);
        while (path_idx != -1) {
            auto const& b = t.branches[path_idx];
            path.insert(path.end(), b.spur_path.cbegin() + spur_idx, b.spur_path.cend() - 1);
            spur_idx = b.spur_idx;
            path_idx = b.parent_id;
        }
        path.push_back(t.branches[0].spur_path.back());
        paths.emplace_back(item.first, path);
    }
    return paths;
}
// Convert an input file to an array of paths.
Paths to_paths(std::istream& stream) {
    Paths       paths;
    std::string line;
    bool        header_not_parsed = true;
    while (std::getline(stream, line)) {
        if (line[0] == '#') {
            continue;
        }
        if (header_not_parsed) {
            header_not_parsed = false;
            continue;
        }
        std::stringstream line_stream(line);
        std::string       cell;
        Cost              cost{0};
        std::getline(line_stream, cell, '\t');
        std::stringstream(cell) >> cost;
        Size size{0};
        std::getline(line_stream, cell, '\t');
        std::stringstream(cell) >> size;
        Nodes nodes(size);
        for (Size i = 0; i != size; ++i) {
            std::getline(line_stream, cell, '\t');
            std::stringstream(cell) >> nodes[i];
        };
        paths.emplace_back(cost, nodes);
    }
    return paths;
}

/**
 * Total weight of the edges of a path.
 */
Cost PWUSGrid::cost(Nodes const& n) const {
    Cost cost = 0;
    for (Size node_idx = 0; node_idx != n.size() - 1; ++node_idx) {
        auto node = n[node_idx];
        auto dir  = direction.find(n[node_idx + 1] - node)->second;
        cost += costs[node][dir];
    }
    return cost;
}

/**
 * A path from source to target that traverses along the outer layer.
 *
 * TODO sometimes it overlaps.
 */
Nodes PWUSGrid::outer_path(Node source, Node target) const {
    auto [x1, y1] = position(source);
    auto [x2, y2] = position(target);
    Len const pad = 1;
    Nodes     nodes(2 * length - 1 + y1 + y2 + x1 - x2 - 6 * pad);
    NodeIdx   n = -1;
    for (Len i = x2; i != length - 1 - pad; ++i) {
        nodes[++n] = node(i, y2);
    }
    for (Len j = y2; j != pad; --j) {
        nodes[++n] = node(length - 1 - pad, j);
    }
    for (Len i = length - 1 - pad; i != pad; --i) {
        nodes[++n] = node(i, pad);
    }
    for (Len j = pad; j != y1; ++j) {
        nodes[++n] = node(pad, j);
    }
    for (Len i = pad; i != x1 + 1; ++i) {
        nodes[++n] = node(i, y1);
    }
    return nodes;
}

/**
 * Simulated annealing
 */
Path PWUSGrid::simulated_annealing(Node source, Node target) const {
    // Seed PRNG with a real random value
    pcg_extras::seed_seq_from<std::random_device> seed;
    // Make a random number engine
    pcg32 prng;  // Use (seed) for random seed or nothing for fixed seed.
    // Discard some states
    prng.discard(1000);

    std::uniform_int_distribution<int>      toss(0, 1);
    std::array<std::array<Dir, 2>, 4> const turn{{{1, 2}, {0, 3}, {0, 3}, {1, 2}}};

    Nodes             path      = outer_path(source, target);
    Cost              path_cost = cost(path);
    std::vector<bool> is_free(size, true);
    for (auto& node : path) {
        is_free[node] = false;
    }

    Size times = 200;
    for (Size t = 0; t != times; ++t) {
        Size N = path.size();
        for (Size n = 0; n != N; ++n) {
            while (true) {
                std::uniform_int_distribution<Node> choose(0, path.size() - 2);
                Size                                node_idx = choose(prng);
                std::array<Node, 2>                 edge{path[node_idx], path[node_idx + 1]};
                auto                path_dir = direction.find(edge[1] - edge[0])->second;
                auto                move_dir = turn[path_dir][toss(prng)];
                auto                move     = node_diff[move_dir];
                std::array<Node, 2> new_edge{edge[0] + move, edge[1] + move};
                enum state { shrink = 0, free = 1 };
                std::array<state, 2> states{shrink, shrink};

                for (auto n : {0, 1}) {
                    if (is_free[new_edge[n]]) {
                        states[n] = free;
                    } else if (
                        node_idx == n * (path.size() - 2) ||
                        edge[n] != path[node_idx - 1 + 3 * n]) {
                        continue;
                    }
                }

                switch (states[0] + 2 * states[1]) {
                    case 0: {  // Two shrinks
                        auto cost = -costs[edge[0]][path_dir] - costs[edge[0]][move_dir] +
                                    costs[new_edge[1]][reverse(path_dir)] -
                                    costs[new_edge[1]][reverse(move_dir)];

                        if (cost < 0) {
                            auto it = path.begin() + node_idx;
                            path.erase(it, it + 2);
                            is_free[edge[0]] = true;
                            is_free[edge[1]] = true;
                            path_cost += cost;
                        }
                        break;
                    }
                    case 1: {  // Tail free, head shrinks
                        auto cost = -costs[edge[0]][path_dir] + costs[edge[0]][move_dir] +
                                    costs[new_edge[1]][reverse(path_dir)] -
                                    costs[new_edge[1]][reverse(move_dir)];

                        if (cost < 0) {
                            path[node_idx + 1]   = new_edge[0];
                            is_free[edge[1]]     = true;
                            is_free[new_edge[0]] = false;
                            path_cost += cost;
                        }
                        break;
                    }
                    case 2: {  // Head free, tail shrinks
                        auto cost = -costs[edge[0]][path_dir] - costs[edge[0]][move_dir] +
                                    costs[new_edge[1]][reverse(path_dir)] +
                                    costs[new_edge[1]][reverse(move_dir)];

                        if (cost < 0) {
                            path[node_idx]       = new_edge[1];
                            is_free[edge[0]]     = true;
                            is_free[new_edge[1]] = false;
                            path_cost += cost;
                        }
                        break;
                    }
                    case 3: {  // Both free
                        if (costs[new_edge[1]][reverse(path_dir)] > 0) {
                            auto cost = -costs[edge[0]][path_dir] + costs[edge[0]][move_dir] +
                                        costs[new_edge[1]][reverse(path_dir)] +
                                        costs[new_edge[1]][reverse(move_dir)];

                            if (cost < 0) {
                                path.insert(
                                    path.begin() + node_idx + 1, {new_edge[0], new_edge[1]});
                                is_free[new_edge[0]] = false;
                                is_free[new_edge[1]] = false;
                                path_cost += cost;
                            }
                        }
                    }
                }
                break;
            }
        }
        std::cout << Path{path_cost, path};
    }
    return {cost(path), path};
}

/**
 * Dijkstra algorithm.
 *
 * @param a_star If true the A* variant is enabled. It will use the heuristic stored by the class
 * instance.
 */
Tree PWUSGrid::dijkstra(Node const source, Node const target, bool const a_star) const {
    // The parents of each node in the single source shortest path tree.
    Nodes parents(size, -1);
    // Minimum costs from source.
    Costs path_costs(size, std::numeric_limits<Cost>::max());
    path_costs[source] = 0;
    // Priority queue of active nodes, sorted by cost.
    std::set<std::pair<Cost, Node>> active_nodes;
    active_nodes.insert({0, source});
    // Algorithm runs until the queue of active nodes is empty.
    while (!active_nodes.empty()) {
        // Select first node in the queue.
        auto n = active_nodes.cbegin()->second;
        // Stop if target node has been reached.
        if (n == target) {
            break;
        }
        // Delete first node from queue.
        active_nodes.erase(active_nodes.cbegin());
        // For each node connected to the selected node...
        auto edges      = neighbours(n);
        auto node_costs = costs[n];
        for (auto const& d : directions) {
            auto to   = edges[d];
            auto cost = node_costs[d];
            // ...is current cost bigger than from the selected node?
            if (cost >= 0 && path_costs[to] > path_costs[n] + cost) {
                active_nodes.erase({path_costs[to], to});  // if yes, erase it from the queue...
                parents[to]    = n;                        // ...update predecessor...
                path_costs[to] = path_costs[n] + cost;     // ...update cost...
                active_nodes.emplace(
                    path_costs[to] + (a_star ? heuristic(to, target) : 0),
                    to);  // ...and reinsert it to the queue.
            }
        }
    }
    return {path_costs, parents};
}

/**
 * Calculate the shortest path from source to node.
 */
Path PWUSGrid::shortest_path(Node const source, Node const target) const {
    return root_path(dijkstra(source, target, true), target);
}

/**
 * Calculate the single source shortest path tree.
 */
Tree PWUSGrid::shortest_path_tree(Node const source) const { return dijkstra(source, -1, false); }

/**
 * Calculate simple shortest paths from source to target.
 */
Trie PWUSGrid::shortest_simple_paths(Node const source, Node const target, Size const paths_count) {
    // Determine the shortest path from the source to the target.
    auto [cost, spur_path] = shortest_path(source, target);
    // Add it to a trie.
    Branches trie;
    trie.reserve(paths_count);
    trie.emplace_back(0, -1, spur_path.size() - 1, spur_path.size(), spur_path);
    // Add it to the array of shortest simple paths.
    std::vector<std::pair<Cost, PathIdx>> selected;
    selected.reserve(paths_count);
    selected.emplace_back(cost, 0);
    // Priority queue of candidates.
    std::set<std::pair<Cost, PathIdx>> candidates;
    // Each loop generates the k-shortest path.
    for (Size k = 1; k != paths_count; ++k) {
        // New candidates are sons of the (k-1)-shortest path.
        auto        parent_id = selected.back().second;
        auto const& parent    = trie[parent_id];
        // Disconnect 1) the nodes of the parent's root path and...
        switch_path(trie, parent.parent_id, parent.spur_idx);
        // 2) the edges of its spur node visited by ancestors.
        switch_prior_spur_edges(trie, parent_id);
        // The spur node traverses the parent's spur path.
        auto next_node   = parent.spur_path.back();
        Dir  dir_to_next = -1;
        auto root_cost   = parent.root_cost;
        bool at_spur     = true;
        for (auto spur_idx = parent.spur_path.size() - 1; spur_idx != 0; --spur_idx) {
            // Move to the next node in the parent's spur path.
            auto spur_node = std::exchange(next_node, parent.spur_path[spur_idx - 1]);
            dir_to_next    = direction.find(next_node - spur_node)->second;
            // Disconnect the edge to the next spur node (because it was visited by the parent).
            switch_edge(spur_node, dir_to_next);
            // Calculate the spur path from the spur node to the target.
            auto [cost, spur_path] = shortest_path(spur_node, target);
            // Was a path found?
            if (spur_path.size() != 1) {
                cost += root_cost;
                // Only store the necessary number of candidates.
                if (candidates.size() != paths_count - k) {
                    trie.emplace_back(
                        root_cost, parent_id, spur_idx,
                        parent.size + spur_path.size() - spur_idx - 1, spur_path);
                    candidates.emplace(cost, trie.size() - 1);
                } else {
                    auto [max_cost, path_id] = *(--candidates.cend());
                    // Is the candidate's cost low enough?
                    if (cost < max_cost) {
                        trie[path_id] = Branch(
                            root_cost, parent_id, spur_idx,
                            parent.size + spur_path.size() - spur_idx - 1, spur_path);
                        candidates.erase(--candidates.cend());
                        candidates.emplace(cost, path_id);
                    }
                }
            }
            if (at_spur) {
                // Reconnect the edges visited by prior siblings of the parent path.
                switch_prior_spur_edges(trie, parent_id);
                at_spur = false;
            }
            // Before moving to the next spur node 1) turn off the current spur node...
            switch_node_except(spur_node, dir_to_next);
            // 2) update the cost of the root path.
            root_cost -= costs[spur_node][dir_to_next];
        }
        // Reconnect parent's path.
        switch_path(trie, parent_id, 0);
        // Exit the loop if all paths have been found.
        if (candidates.empty()) {
            break;
        }
        // Select the shortest candidate.
        auto shortest = candidates.cbegin();
        selected.push_back(*shortest);
        candidates.erase(shortest);
    }
    return {selected, trie};
}

/**
 * Overlap
 */
Size overlap(Nodes const& nodes1, Nodes const& nodes2) {
    Size count = 0;
    auto begin = nodes2.begin();
    for (Size i = 0; i != nodes1.size() - 1; ++i) {
        auto match = std::find(begin, nodes2.end(), nodes1[i]);
        if (match != nodes2.end() &&
            (nodes1[i + 1] == *(match + 1) || nodes1[i + 1] == *(match - 1))) {
            ++count;
            begin = match - 1;
        } else {
            begin = nodes2.begin();
        }
    }
    return count;
}
Size overlap(Path const& path1, Path const& path2) { return overlap(path1.nodes, path2.nodes); }

// Outputs

std::ostream& operator<<(std::ostream& os, Position const& obj) {
    os << "(" << obj.coordinate_x << ',' << obj.coordinate_y << ")";
    return os;
}
std::ostream& operator<<(std::ostream& os, Nodes const& obj) {
    os << "'size'\t'nodes'\n";
    os << obj.size();
    for (auto const& item : obj) {
        os << '\t' << item;
    }
    os << '\n';
    return os;
}
std::ostream& operator<<(std::ostream& os, Path const& obj) {
    os << "'cost'\t'size'\t'nodes'\n" << std::setprecision(12);
    os << obj.cost << '\t' << obj.nodes.size();
    for (auto const& item : obj.nodes) {
        os << '\t' << item;
    }
    os << '\n';
    return os;
}
std::ostream& operator<<(std::ostream& os, Paths const& obj) {
    os << "'cost'\t'size'\t'nodes'\n" << std::setprecision(12);
    for (auto const& item : obj) {
        os << item.cost << '\t' << item.nodes.size();
        for (auto const& it : item.nodes) {
            os << '\t' << it;
        }
        os << '\n';
    }
    return os;
}
std::ostream& operator<<(std::ostream& os, Branch const& obj) {
    os << "'root cost'\t'parent id'\t'spur index'\t'spur path'\n" << std::setprecision(12);
    os << obj.root_cost << '\t' << obj.parent_id << '\t' << obj.spur_idx;
    for (auto const& item : obj.spur_path) {
        os << '\t' << item;
    }
    os << '\n';
    return os;
}
std::ostream& operator<<(std::ostream& os, Branches const& obj) {
    os << "'root cost'\t'parent id'\t'spur index'\t'size'\t'spur path'\n" << std::setprecision(12);
    for (auto const& item : obj) {
        os << item.root_cost << '\t' << item.parent_id << '\t' << item.size << '\t'
           << item.spur_idx;
        for (auto const& it : item.spur_path) {
            os << '\t' << it;
        }
        os << '\n';
    }
    return os;
}
std::ostream& operator<<(std::ostream& os, Trie const& obj) {
    os << "'cost'\t'size'\t'nodes'\n" << std::setprecision(12);
    for (auto const& item : obj.costs) {
        os << item.first << '\t' << obj.branches[item.second].size;
        Size spur_idx = 0;
        auto path_idx = item.second;
        while (path_idx != -1) {
            auto const& branch = obj.branches[path_idx];
            for (Size n = spur_idx; n != branch.spur_path.size(); ++n) {
                os << '\t' << branch.spur_path[n];
            }
            spur_idx = branch.spur_idx + 1;
            path_idx = branch.parent_id;
        }
        os << '\n';
    }
    return os;
}
std::ostream& operator<<(std::ostream& os, PWUSGrid const& obj) {
    os << "'from'\t'to'\t'weight'\n" << std::setprecision(12);
    int const pad = 1;
    for (Len j = pad; j != obj.length - pad; ++j) {
        for (Len i = pad; i != obj.length - pad; ++i) {
            auto n = obj.node(i, j);
            for (auto const& d : directions) {
                auto const cost = obj.costs[n][d];
                if (cost > 0) {
                    os << n << '\t' << obj.neighbours(n)[d] << '\t' << cost << '\n';
                }
            }
        }
    }
    return os;
}

}  // namespace pwus_grid