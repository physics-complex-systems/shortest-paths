#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono>
#include <iomanip>
#include <iostream>

namespace timer {

std::string iso_time_format(double seconds);

/**
 * Timer
 *
 * Time processes.
 *
 * Example - continuous measurement:
 *
 *     Timer timer;
 *     std::cout << timer;
 *
 * Example - discontinuous measurement:
 *
 *     Timer timer;
 *     timer.start();
 *     ...
 *     timer.log();
 *     std::cout << timer;
 */
class Timer {
    using Clock  = std::chrono::high_resolution_clock;
    using Second = std::chrono::duration<double, std::ratio<1>>;

   public:
    [[nodiscard]] double elapsed() const { return current() + logged; }

    void log() {
        logged += current();
        start();
    }

    /**
     * Print progress
     *
     * Monitor an iterative process. A progress bar is printed in the console.
     *
     * @param done  Completed iterations.
     * @param total Total iterations.
     */
    void progress(std::size_t done, std::size_t total) {
        int const bars_count = 20;
        double    ratio      = done / static_cast<double>(total);
        auto      bars       = static_cast<int>(ratio * bars_count);
        std::cout << "\r[" << std::string(bars, '#') << std::string(bars_count - bars, ' ') << ']';
        std::cout << std::setw(3) << static_cast<int>(100 * ratio) << "%";
        std::cout << ", elapsed: " << iso_time_format(elapsed());
        std::cout << ", remaining: " << iso_time_format((1 / ratio - 1) * elapsed());
        if (done == total) {
            std::cout << std::endl;
        } else {
            std::cout.flush();
        }
    }

    void reset() {
        logged = 0;
        start();
    }

    void start() { checkpoint = Clock::now(); }

   private:
    [[nodiscard]] double current() const {
        return std::chrono::duration_cast<Second>(Clock::now() - checkpoint).count();
    }
    std::chrono::time_point<Clock> checkpoint{Clock::now()};
    double                         logged{0};
};

std::ostream& operator<<(std::ostream& os, Timer const& timer);

}  // namespace timer

#endif  // TIMER_HPP