#ifndef WELFORD_HPP
#define WELFORD_HPP

#include <chrono>
#include <iomanip>
#include <iostream>

namespace welford {

template<typename T>
void welford_update(
    T new_value, T& mean, T& variance, std::size_t current_count, std::size_t total_count) {
    auto delta1 = new_value - mean;
    mean += delta1 / current_count;
    auto delta2 = new_value - mean;
    variance += delta1 * delta2;
    if (current_count == total_count) {
        variance /= (current_count - 1);
    }
}

}  // namespace welford

#endif  // WELFORD_HPP