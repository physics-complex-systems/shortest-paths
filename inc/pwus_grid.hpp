#ifndef PWUS_GRID_HPP
#define PWUS_GRID_HPP

#include <algorithm>
#include <array>
#include <iomanip>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <vector>

#include "pcg_random.hpp"

namespace pwus_grid {

/**
 * Adimensional
 */
using Size    = std::size_t;
using NodeIdx = int32_t;
using PathIdx = int32_t;

/**
 * Grid directions
 */
using Dir = int8_t;                                          ///< A direction.
inline std::array<Dir, 2> constexpr axes{0, 1};              ///< 0=Vertical, 1=Horizontal.
inline std::array<Dir, 4> constexpr directions{0, 1, 2, 3};  ///< 0=Down, 1=Left, 2=Right, 3=Up.

/**
 * Grid distances
 */
using Len = NodeIdx;  ///< Length (number of grid hops).
struct Position {
    Len coordinate_x;
    Len coordinate_y;
};

/**
 * Nodes
 */
using Node       = NodeIdx;              ///< A node of the grid.
using Neighbours = std::array<Node, 4>;  ///< Nodes adjacent to a node.
using Nodes      = std::vector<Node>;

/**
 * Weights
 */
using Cost      = double;               ///< Weight associated to an edge.
using NodeCosts = std::array<Cost, 4>;  ///< Weights associated to the neighbours of a node.
using Costs     = std::vector<Cost>;
using Grid      = std::vector<NodeCosts>;
static_assert(std::is_signed<Cost>::value, "Weights must be signed to (dis)connect edges.");

/**
 * Path
 *
 * A sequence of distinct nodes, each adjacent to its child and parent.
 */
struct Path {
    Path(Cost const cost, Nodes nodes) : cost{cost}, nodes{std::move(nodes)} {}
    Cost  cost;   ///< The total cost of the edges of the path.
    Nodes nodes;  ///< Nodes are ordered from children to parents.
};
using Paths = std::vector<Path>;

/**
 * Tree
 *
 * A connected acyclic undirected graph represented as a vector whose nth value is the parent of the
 * nth node.
 */
struct Tree {
    Costs costs;
    Nodes parents;
};

/**
 * A branch of a trie.
 *
 * Each branch is defined by its nodes (spur path) and its parent branch (root path).
 *
 * To aid calculations, the index at which it branches from its parent is stored, as well as the
 * total cost of the root path.
 */
struct Branch {
    Branch(
        Cost const root_cost, PathIdx const parent_id, NodeIdx const spur_idx, Size const size,
        Nodes spur_path) :
        root_cost{root_cost},
        parent_id{parent_id},
        spur_idx{spur_idx},
        size{size},
        spur_path{std::move(spur_path)} {}
    Cost    root_cost;
    PathIdx parent_id;
    NodeIdx spur_idx;
    Size    size;
    Nodes   spur_path;
};
using Branches = std::vector<Branch>;

/**
 * Trie
 *
 * A tree represented as a set of branches.
 */
struct Trie {
    std::vector<std::pair<Cost, PathIdx>> costs;
    Branches                              branches;
};

/**
 * Positive Weighted Undirected Square (PWUS) Grid.
 *
 * Use the randomize method to randomize the weights of the edges. The
 * boundary will be inaccesible to allow homogeneous (faster) calculations.
 *
 * @param length Length of the grid's side.
 */
class PWUSGrid {
   public:
    explicit PWUSGrid(Len const length) :
        length{length},
        size{length * length},
        node_diff{-length, -1, 1, length},
        direction{{-length, 0}, {-1, 1}, {1, 2}, {length, 3}},
        costs(size, {-1, -1, -1, -1}) {
        std::fill(min_costs.begin(), min_costs.end(), Costs(length - 2));
    }

    // Static geometry
    [[nodiscard]] Position   position(Node node) const;
    [[nodiscard]] Node       node(Len coordinate_x, Len coordinate_y) const;
    [[nodiscard]] Node       neighbour(Node n, Dir d) const;
    [[nodiscard]] Neighbours neighbours(Node n) const;
    [[nodiscard]] Cost       cost(Nodes const& n) const;

    // Dynamic geometry
    template<typename Distribution>
    void randomize(Distribution& distribution);
    template<typename Distribution, typename Generator>
    void randomize(Distribution& distribution, Generator& generator);

    // Algorithms
    [[nodiscard]] Nodes outer_path(Node source, Node target) const;
    [[nodiscard]] Path  simulated_annealing(Node source, Node target) const;
    [[nodiscard]] Path  shortest_path(Node source, Node target) const;
    [[nodiscard]] Tree  shortest_path_tree(Node source) const;
    [[nodiscard]] Trie  shortest_simple_paths(Node source, Node target, Size paths_count);

    // Print
    [[nodiscard]] Grid   get_costs() const { return costs; }
    friend std::ostream& operator<<(std::ostream&, PWUSGrid const&);

   private:
    // Dimensions
    Len const length;  ///< Length of the grid's side.
    Len const size;    ///< Size of the grid (number of nodes).
    // Direction (2D step) -> Node index difference between neighbours (1D step)
    std::array<Len, 4> const node_diff;  // The index is a Direction
    // 1D step -> 2D step.
    std::map<Len, Dir> const direction;  // The key is a Node1 - Node2 for neighbours.

    // Dynamic geometry
    void switch_edge(Node tail, Dir d);
    void switch_edge(Node tail, Node head);
    void switch_node(Node tail);
    void switch_node_except(Node tail, Dir except);
    void turn_node_off(Node tail);
    void switch_prior_spur_edges(Branches const& b, PathIdx p);
    void switch_path(Branches const& b, PathIdx p, Size spur_idx);

    // Algorithms
    [[nodiscard]] Cost heuristic(Node n1, Node n2) const;
    [[nodiscard]] Tree dijkstra(Node source, Node target, bool a_star) const;

    // Data members

    // Weights
    Grid costs;
    // The sum of the smallest edges for each length. Used by the A* algorithm.
    std::array<Costs, 2> min_costs;
};

/**
 * Down <-> Up, Left <-> Right.
 */
inline Dir reverse(Dir d) { return 3 - d; }
Path       root_path(Tree const& t, Node leaf);
Paths      to_paths(Trie const& t);
Paths      to_paths(std::istream& stream);

inline Position PWUSGrid::position(Node node) const { return {node % length, node / length}; }

inline Node PWUSGrid::node(Len const coordinate_x, Len const coordinate_y) const {
    return coordinate_x + length * coordinate_y;
}

// Homogeneous for inner nodes, incorrect for padding nodes
inline Node PWUSGrid::neighbour(Node const n, Dir const d) const { return n + node_diff[d]; }

// Homogeneous for inner nodes, incorrect for padding nodes
inline Neighbours PWUSGrid::neighbours(Node const n) const {
    return {n - length, n - 1, n + 1, n + length};
}

inline void PWUSGrid::switch_edge(Node const tail, Dir const d) { costs[tail][d] *= -1; }
inline void PWUSGrid::switch_edge(Node const tail, Node const head) {
    auto const d = direction.find(head - tail)->second;
    costs[tail][d] *= -1;
}
inline void PWUSGrid::switch_node(Node const tail) {
    for (auto const& d : directions) {
        switch_edge(tail, d);
    }
}
inline void PWUSGrid::switch_node_except(Node const tail, Dir const except) {
    for (auto const& d : directions) {
        if (d != except) {
            switch_edge(tail, d);
        }
    }
}
inline void PWUSGrid::switch_prior_spur_edges(Branches const& b, PathIdx p) {
    while (p != 0) {
        Size spur_idx    = b[p].spur_idx;
        p                = b[p].parent_id;
        auto const& path = b[p].spur_path;
        switch_edge(path[spur_idx], path[spur_idx - 1]);
        if (spur_idx != path.size() - 1) {
            break;
        }
    }
}
// Switch nodes in the parent's root, and edges coming from the spur node.
inline void PWUSGrid::switch_path(Branches const& b, PathIdx p, Size spur_idx) {
    while (p != -1) {
        auto const& path = b[p].spur_path;
        for (auto n = path.size() - 1; n != spur_idx; --n) {
            switch_node(path[n]);
        }
        spur_idx = b[p].spur_idx;
        p        = b[p].parent_id;
    }
}

inline Cost PWUSGrid::heuristic(Node const n1, Node const n2) const {
    auto [x1, y1] = position(n1);
    auto [x2, y2] = position(n2);
    return min_costs[0][std::abs(y1 - y2)] + min_costs[1][std::abs(x1 - x2)];
}

template<typename Distribution, typename Generator>
void PWUSGrid::randomize(Distribution& distribution, Generator& generator) {
    // Reset heuristic for the A* algorithm
    for (auto d : axes) {
        std::fill(min_costs[d].begin(), min_costs[d].end(), std::numeric_limits<Cost>::max());
        min_costs[d].back() = 0;
        std::make_heap(min_costs[d].begin(), min_costs[d].end());
    }
    // Randomize edges
    for (Len j = 1; j != length - 1; ++j) {
        for (Len i = 1; i != length - 1; ++i) {
            auto n = node(i, j);
            for (auto d : axes) {
                if ((d ? i : j) != 1) {
                    // Update PRNG
                    Cost cost = distribution(generator);
                    // Update heuristic
                    if (cost < min_costs[d].front()) {
                        std::pop_heap(min_costs[d].begin(), min_costs[d].end());
                        min_costs[d].back() = cost;
                        std::push_heap(min_costs[d].begin(), min_costs[d].end());
                    }
                    // Update costs
                    costs[n][d]                        = cost;
                    costs[neighbour(n, d)][reverse(d)] = cost;
                }
            }
        }
    }
    // Calculate heuristic
    for (auto d : axes) {
        std::sort_heap(min_costs[d].begin(), min_costs[d].end());
        std::partial_sum(min_costs[d].begin(), min_costs[d].end(), min_costs[d].begin());
    }
}
template<typename Distribution>
void PWUSGrid::randomize(Distribution& distribution) {
    // Seed PRNG with a real random value
    pcg_extras::seed_seq_from<std::random_device> seed;
    // Make a random number engine
    pcg32 prng(seed);  //> Use (seed) for random seed or nothing for fixed seed.
    // Discard some states
    prng.discard(1000);
    // Randomize
    randomize(distribution, prng);
}

// Overlap
Size overlap(Nodes const&, Nodes const&);
Size overlap(Path const&, Path const&);

// Outputs

std::ostream& operator<<(std::ostream&, Position const&);
std::ostream& operator<<(std::ostream&, Nodes const&);
std::ostream& operator<<(std::ostream&, Path const&);
std::ostream& operator<<(std::ostream&, Paths const&);
std::ostream& operator<<(std::ostream&, Branch const&);
std::ostream& operator<<(std::ostream&, Branches const&);
std::ostream& operator<<(std::ostream&, Trie const&);
std::ostream& operator<<(std::ostream&, PWUSGrid const&);

}  // namespace pwus_grid

#endif  // PWUS_GRID_HPP