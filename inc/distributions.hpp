#ifndef DISTRIBUTIONS_HPP
#define DISTRIBUTIONS_HPP

#include <cmath>
#include <iomanip>
#include <iostream>
#include <random>
#include <string>

namespace distributions {

class ParetoDistribution {
   public:
    ParetoDistribution(double scale, double shape) : scale{scale}, shape{shape} {}

    double scale;
    double shape;

    template<class Generator>
    double operator()(Generator& g) {
        std::uniform_real_distribution<double> uniform(0.0, 1.0);
        double                                 cdf = uniform(g);
        return scale * std::pow(cdf, -1 / shape);
    }
};

std::string name(ParetoDistribution);
template<typename T>
std::string name(std::uniform_real_distribution<T>) {
    return "uniform";
}

std::string params(ParetoDistribution dist, int precision);
template<typename T>
std::string params(std::uniform_real_distribution<T> dist, int precision) {
    std::stringstream ss;
    ss << std::setprecision(precision);
    ss << "(min:" << dist.min() << ",max:" << dist.max() << ")";
    return ss.str();
}
template<typename Distribution>
std::string params(Distribution dist) {
    return params(dist, 12);
}

double mean(ParetoDistribution dist);
template<typename T>
double mean(std::uniform_real_distribution<T> dist) {
    return (dist.min() + dist.max()) / 2;
}

double var(ParetoDistribution dist);
template<typename T>
double var(std::uniform_real_distribution<T> dist) {
    return pow(dist.max() - dist.min(), 2) / 12;
}

}  // namespace distributions

#endif  // DISTRIBUTIONS_HPP