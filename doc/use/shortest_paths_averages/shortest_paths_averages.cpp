#include <filesystem>
#include <fstream>

#include "distributions.hpp"
#include "pwus_grid.hpp"
#include "timer.hpp"
#include "welford.hpp"

int main() {
    // Link distributions (uncomment one)
    int const critical_distance = 2;
    // Uniform
    double                                 min = 1 - 1 / sqrt(critical_distance);
    double                                 max = 2 - min;
    std::uniform_real_distribution<double> distribution(min, max);
    // Pareto
    // double                            shape = 1 + sqrt(3 * critical_distance + 1);
    // double                            scale = 1 - 1 / shape;
    // distributions::ParetoDistribution distribution(scale, shape);

    // Other free parameters
    int const direction   = 1;  //> 0 axis, 1 diagonal
    int const distance    = 32;
    int const paths_count = 5000;
    int const iterations  = 4000;
    // Do-not-touch parameters
    int const margin   = 2;  //> To allow paths that go around source or target.
    int const boundary = 1;  //> Required by the representation of the graph.

    // Base filename
    std::string       direction_name = direction == 0 ? "axis" : "diag";
    std::stringstream filename;
    filename << "doc/use/shortest_paths_averages/out/";
    filename << direction_name << '_';
    filename << distributions::name(distribution) << '_';
    filename << "critical" << critical_distance;
    filename << "distance" << distance;
    filename << "paths" << paths_count;
    filename << "iterations" << iterations;
    // Checks
    std::filesystem::path file{filename.str() + "_params.csv"};
    if (std::filesystem::exists(file)) {
        return 0;
    }
    // Record parameters
    std::ofstream out(filename.str() + "_params.csv");
    out << "direction\tdistribution\tmean\tvariance\tcritical_distance\t"
           "distance\tpaths_count\titerations\n";
    out << direction_name << '\t' << distributions::name(distribution)
        << distributions::params(distribution) << '\t' << distributions::mean(distribution) << '\t'
        << distributions::var(distribution) << '\t' << critical_distance << '\t' << distance << '\t'
        << paths_count << '\t' << iterations;
    out.close();

    // Timing
    timer::Timer timer;
    // PRNG
    pcg_extras::seed_seq_from<std::random_device> seed;
    pcg32                                         prng(seed);

    // Storage
    std::array<double, paths_count> cost_mean;
    std::array<double, paths_count> cost_var;
    std::array<double, paths_count> first_overlap_mean;
    std::array<double, paths_count> first_overlap_var;
    cost_mean.fill(0);
    cost_var.fill(0);
    first_overlap_mean.fill(0);
    first_overlap_var.fill(0);

    // Generate k shortest paths
    int const           pad    = margin + boundary;
    int const           length = 2 * (distance + pad) + 1;
    pwus_grid::PWUSGrid grid(length);  //> Build grid
    int const           source = grid.node(pad, pad + (1 - direction) * distance / 2);
    int const           target = source + distance * (1 + direction * length);
    // Iterate
    for (std::size_t i = 0; i != iterations; ++i) {
        // Randomize distribution
        grid.randomize(distribution, prng);
        // Find shortest paths
        auto paths = to_paths(grid.shortest_simple_paths(source, target, paths_count));
        for (std::size_t k = 0; k != paths_count; ++k) {
            // Cost
            welford::welford_update(paths[k].cost, cost_mean[k], cost_var[k], i + 1, iterations);
            // Overlap with shortest path
            double overlap = pwus_grid::overlap(paths[0], paths[k]);
            overlap /=
                static_cast<double>(std::min(paths[0].nodes.size(), paths[k].nodes.size())) - 1;
            welford::welford_update(
                overlap, first_overlap_mean[k], first_overlap_var[k], i + 1, iterations);
        }
        timer.progress(i + 1, iterations);
    }

    // Outputs
    out.open(filename.str() + "_averages.csv");
    out << std::setprecision(12)
        << "shortest_path_rank\tcost_mean\tcost_variance\t"
           "first_overlap_mean\tfirst_overlap_variance\n";
    for (std::size_t k = 0; k != paths_count; ++k) {
        out << k + 1 << '\t' << cost_mean[k] << '\t' << cost_var[k] << '\t' << first_overlap_mean[k]
            << '\t' << first_overlap_var[k] << '\n';
    }
    out.close();

    return 0;
}