#include <filesystem>
#include <fstream>

#include "distributions.hpp"
#include "pwus_grid.hpp"
#include "timer.hpp"
#include "welford.hpp"

int main() {
    // Link distributions (uncomment one)
    int const critical_distance = 2;
    // Uniform
    double                                 min = 1 - 1 / sqrt(critical_distance);
    double                                 max = 2 - min;
    std::uniform_real_distribution<double> distribution(min, max);
    // Pareto
    // double                            shape = 1 + sqrt(3 * critical_distance + 1);
    // double                            scale = 1 - 1 / shape;
    // distributions::ParetoDistribution distribution(scale, shape);

    // Distances - inputs
    int const max_distance    = 100;
    int const min_distance    = 1;
    int const distances_count = 41;
    // Distances - distribute uniformly in log scale
    std::set<int> distances;
    double const  step = (log10(max_distance) - log10(min_distance)) / (distances_count - 1);
    for (int i = 0; i != distances_count; ++i) {
        auto temp = std::round(pow(10, log10(min_distance) + step * i));
        if (temp > 71) {
            distances.insert(temp);
        }
    }
    // Other free parameters
    int const direction   = 1;  //> 0 axis, 1 diagonal
    int const paths_count = 1000;
    int const iterations  = 4000;
    // Do-not-touch parameters
    int const margin   = 2;  //> To allow paths that go around source or target.
    int const boundary = 1;  //> Required by the representation of the graph.

    // Base filename
    std::string       direction_name = direction == 0 ? "axis" : "diag";
    std::stringstream filename;
    filename << "doc/use/shortest_paths_vs_distance/out/";
    filename << direction_name << '_';
    filename << distributions::name(distribution);
    filename << "critical" << critical_distance;
    filename << "distances" << min_distance << "-" << max_distance;
    filename << "paths" << paths_count;
    filename << "iterations" << iterations << "_ver2_";
    // Checks
    std::filesystem::path file{filename.str() + "_params.csv"};
    if (std::filesystem::exists(file)) {
        return 0;
    }
    // Record parameters
    std::ofstream out(filename.str() + "_params.csv");
    out << "direction\tdistribution\tmean\tvariance\tcritical_distance\t"
           "distances\tpaths_count\titerations\n";
    out << direction_name << '\t' << distributions::name(distribution)
        << distributions::params(distribution) << '\t' << distributions::mean(distribution) << '\t'
        << distributions::var(distribution) << '\t' << critical_distance << '\t' << min_distance
        << '-' << max_distance << '\t' << paths_count << '\t' << iterations;
    out.close();
    // Output
    out.open(filename.str() + "_vs_distance.csv");
    out << "distance\tshortest_path_rank\tcost_mean\tcost_variance";

    // Timing
    timer::Timer timer;
    // PRNG
    pcg_extras::seed_seq_from<std::random_device> seed;
    pcg32                                         prng(seed);

    for (auto distance : distances) {
        // Storage
        std::array<double, paths_count> cost_mean;
        std::array<double, paths_count> cost_var;
        cost_mean.fill(0);
        cost_var.fill(0);
        // Main
        int const           pad    = margin + boundary;
        int const           length = distance + 2 * pad + 1;
        pwus_grid::PWUSGrid grid(length);  //> Build grid
        int const           source = grid.node(pad, pad + (1 - direction) * distance / 2);
        int const           target = source + distance * (1 + direction * length);
        for (std::size_t i = 0; i != iterations; ++i) {
            // Calculations
            grid.randomize(distribution, prng);
            auto costs = grid.shortest_simple_paths(source, target, paths_count).costs;
            for (int k = 0; k != paths_count; ++k) {
                welford::welford_update(
                    costs[k].first, cost_mean[k], cost_var[k], i + 1, iterations);
            }
            // Timer
            timer.progress(i + 1, iterations);
        }
        // Output
        for (int k = 0; k != paths_count; ++k) {
            out << std::setprecision(12) << '\n'
                << distance << '\t' << k + 1 << '\t' << cost_mean[k] << '\t' << cost_var[k];
        }
        out.flush();
    }
    out.close();

    return 0;
}