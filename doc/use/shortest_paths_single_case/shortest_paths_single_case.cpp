#include <filesystem>
#include <fstream>

#include "distributions.hpp"
#include "pwus_grid.hpp"
#include "timer.hpp"

using pwus_grid::operator<<;

int main() {
    // Link distributions (uncomment one)
    int const critical_distance = 2;
    // Uniform
    double                                 min = 1 - 1 / sqrt(critical_distance);
    double                                 max = 2 - min;
    std::uniform_real_distribution<double> distribution(min, max);
    // Pareto
    // double                            shape = 1 + sqrt(3 * critical_distance + 1);
    // double                            scale = 1 - 1 / shape;
    // distributions::ParetoDistribution distribution(scale, shape);

    // Other free parameters
    int const direction   = 0;  //> 0 axis, 1 diagonal
    int const distance    = 8;
    int const paths_count = 1000;
    // Do-not-touch parameters
    int const margin   = 2;  //> To allow paths that go around source or target.
    int const boundary = 1;  //> Required by the representation of the graph.

    // Base filename
    std::string       direction_name = direction == 0 ? "axis" : "diag";
    std::stringstream filename;
    filename << "doc/use/shortest_paths_single_case/out/";
    filename << direction_name << '_';
    filename << distributions::name(distribution) << '_';
    filename << "critical" << critical_distance;
    filename << "distance" << distance;
    filename << "paths" << paths_count;
    // Checks
    std::filesystem::path file{filename.str() + "_params.csv"};
    if (std::filesystem::exists(file)) {
        return 0;
    }
    // Record parameters
    std::ofstream out(filename.str() + "_params.csv");
    out << "direction\tdistribution\tmean\tvariance\tcritical_distance\t"
        << "distance\tpaths_count\n";
    out << direction_name << '\t' << distributions::name(distribution)
        << distributions::params(distribution) << '\t' << distributions::mean(distribution) << '\t'
        << distributions::var(distribution) << '\t' << critical_distance << '\t' << distance << '\t'
        << paths_count;
    out.close();

    timer::Timer timer;

    // Generate k shortest paths
    int const           pad    = margin + boundary;
    int const           length = distance + 2 * pad + 1;
    pwus_grid::PWUSGrid grid(length);  //> Build grid
    int const           source = grid.node(pad, pad + (1 - direction) * distance / 2);
    int const           target = source + distance * (1 + direction * length);
    grid.randomize(distribution);  //> Randomize distribution
    // Find shortest paths
    auto paths = to_paths(grid.shortest_simple_paths(source, target, paths_count));
    out.open(filename.str() + "_paths.csv");
    out << paths;
    out.close();

    // Generate and export overlaps
    out.open(filename.str() + "_overlaps.csv");
    for (std::size_t row = 0; row != paths_count; ++row) {
        for (std::size_t col = 0; col != paths_count; ++col) {
            double overlap = pwus_grid::overlap(paths[row], paths[col]);
            overlap /=
                static_cast<double>(std::min(paths[row].nodes.size(), paths[col].nodes.size())) - 1;
            out << (col == 0 ? "" : ",") << std::setprecision(6) << overlap;
        }
        out << '\n';
    }
    out.close();

    std::cout << timer << std::endl;

    return 0;
}