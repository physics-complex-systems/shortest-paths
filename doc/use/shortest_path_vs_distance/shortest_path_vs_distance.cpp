#include <filesystem>
#include <fstream>

#include "distributions.hpp"
#include "pwus_grid.hpp"
#include "timer.hpp"
#include "welford.hpp"

int main() {
    // Link distributions (uncomment one)
    int const critical_distance = 32;
    // Uniform
    double                                 min = 1 - 1 / sqrt(critical_distance);
    double                                 max = 2 - min;
    std::uniform_real_distribution<double> distribution(min, max);
    // Pareto
    // double                            shape = 1 + sqrt(3 * critical_distance + 1);
    // double                            scale = 1 - 1 / shape;
    // distributions::ParetoDistribution distribution(scale, shape);

    // Other free parameters
    int const max_distance = 100;
    int const iterations   = 1000;
    // Do-not-touch parameters
    int const min_distance = 1;  //> Do not change, values other than 1 not fully implemented.
    int const margin       = 2;  //> To allow paths that go around source or target.
    int const boundary     = 1;  //> Required by the representation of the graph.

    // Base filename
    std::stringstream filename;
    filename << "doc/use/shortest_path_vs_distance/out/";
    filename << distributions::name(distribution) << '_';
    filename << "critical" << critical_distance;
    filename << "distances" << min_distance << "-" << max_distance;
    filename << "iterations" << 4 * iterations;
    // Checks
    std::filesystem::path file{filename.str() + "_params.csv"};
    if (std::filesystem::exists(file)) {
        return 0;
    }
    // Record parameters
    std::ofstream out(filename.str() + "_params.csv");
    out << "distribution\tmean\tvariance\tcritical_distance\tdistances\titerations\n";
    out << 4 * iterations << '\t' << min_distance << "-" << max_distance << '\t'
        << critical_distance << '\t' << distributions::name(distribution)
        << distributions::params(distribution) << '\t' << distributions::mean(distribution) << '\t'
        << distributions::var(distribution);
    out.close();

    // Timing
    timer::Timer timer;
    // PRNG
    pcg_extras::seed_seq_from<std::random_device> seed;
    pcg32                                         prng(seed);

    // Storage
    int const                           distances_count = max_distance - min_distance + 1;
    std::array<double, distances_count> axis_cost_mean;
    std::array<double, distances_count> axis_cost_var;
    std::array<double, distances_count> diag_cost_mean;
    std::array<double, distances_count> diag_cost_var;
    axis_cost_mean.fill(0);
    axis_cost_var.fill(0);
    diag_cost_mean.fill(0);
    diag_cost_var.fill(0);

    // Main
    int const           pad    = margin + boundary;
    int const           length = 2 * (max_distance + pad) + 1;
    pwus_grid::PWUSGrid grid(length);  //> Build grid
    int const           mid    = (length - 1) / 2;
    int const           source = grid.node(mid, mid);
    for (std::size_t i = 0; i != iterations; ++i) {
        // Artifacts
        int const          total_count = 4 * iterations;
        std::array<int, 4> axis{+1, +length, -1, -length};
        std::array<int, 4> diag{+1 + length, +length - 1, -1 - length, -length + 1};
        // Calculations
        grid.randomize(distribution, prng);
        auto costs = grid.shortest_path_tree(source).costs;
        for (std::size_t d = 0; d != distances_count; ++d) {
            for (std::size_t j = 0; j != 4; ++j) {
                welford::welford_update(
                    costs[source + (min_distance + d) * axis[j]], axis_cost_mean[d],
                    axis_cost_var[d], 4 * i + j + 1, total_count);
                welford::welford_update(
                    costs[source + (min_distance + d) * diag[j]], diag_cost_mean[d],
                    diag_cost_var[d], 4 * i + j + 1, total_count);
            }
        }
        timer.progress(i + 1, iterations);
    }

    // Outputs
    out.open(filename.str() + "_vs_distance.csv");
    out << "direction\tdistance\tcost_mean\tcost_variance";
    for (std::size_t d = 0; d != distances_count; ++d) {
        out << std::setprecision(12) << '\n'
            << "axis" << '\t' << min_distance + d << '\t' << axis_cost_mean[d] << '\t'
            << axis_cost_var[d] << '\n'
            << "diag" << '\t' << min_distance + d << '\t' << diag_cost_mean[d] << '\t'
            << diag_cost_var[d];
    }
    out.close();

    return 0;
}