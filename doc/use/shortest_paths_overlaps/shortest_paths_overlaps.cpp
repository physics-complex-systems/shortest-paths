#include <cmath>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "pwus_grid.hpp"
#include "timer.hpp"

int main() {
    // Parameters
    int const iterations      = 1000;
    int const paths_count     = 500;
    int const pad             = 3;  //> margin + 1 for boundary
    int const length          = 21 + 2 * pad;
    int const source          = length * (length - 1) / 2 + pad;
    int const target          = length * (length + 1) / 2 - pad - 1;
    int const critical_length = 2;

    // Links distribution - Uniform
    std::string                            distribution_name = "uniform";
    double                                 min               = 1 - 1 / sqrt(critical_length);
    double                                 max               = 2 - min;
    std::uniform_real_distribution<double> distribution(min, max);
    // Links distribution - Pareto
    // std::string                            distribution_name = "pareto";
    // double                                 min               = 1 - 1 / sqrt(critical_length);
    // std::uniform_real_distribution<double> distribution(min, 2 - min);

    // Base filename
    std::stringstream ss;
    ss << "doc/use/out/hh";
    ss << "iterations" << iterations;
    ss << "paths" << paths_count;
    ss << "length" << length - 2 * pad;
    ss << "critical" << critical_length;
    ss << distribution_name;
    std::string filename = ss.str();

    // Checks
    std::filesystem::path file{filename + "_overlaps.csv"};
    if (std::filesystem::exists(file)) {
        return 0;
    }

    // Generate k shortest paths
    pwus_grid::PWUSGrid grid(length);  //> Build grid
    // Outputs
    std::ofstream out_overlaps(filename + "_overlaps.csv");
    // PRNG
    pcg_extras::seed_seq_from<std::random_device> seed;
    pcg32                                         prng(seed);
    // Iterate
    timer::Timer timer;
    for (std::size_t n = 0; n != iterations; ++n) {
        // Randomize distribution
        grid.randomize(distribution, prng);
        // Find shortest paths
        auto paths = to_paths(grid.shortest_simple_paths(source, target, paths_count));
        // Overlaps
        for (std::size_t i = 0; i != paths_count - 1; ++i) {
            for (std::size_t j = i + 1; j != paths_count; ++j) {
                double overlap = pwus_grid::overlap(paths[i], paths[j]);
                overlap *=
                    2 / static_cast<double>(paths[i].nodes.size() + paths[j].nodes.size() - 2);
                out_overlaps << (i + j == 1 ? "" : ",") << std::setprecision(6) << overlap;
            }
        }
        out_overlaps << '\n';
        timer.progress(n + 1, iterations);
    }
    std::cout << std::endl;
    out_overlaps.close();

    return 0;
}