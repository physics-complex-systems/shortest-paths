Changelog
=========

All notable changes to this project will be documented in this section.

This project adheres to `Semantic Versioning`__, and the format is based on `Keep a Changelog`__.

.. __: https://semver.org/spec/v2.0.0.html
.. __: https://keepachangelog.com/en/1.0.0/

major.minor.patch (yyyy-mm-dd)
------------------------------

* ``Added`` for new features.
* ``Changed`` for changes in existing functionality.
* ``Deprecated`` for soon-to-be removed features.
* ``Removed`` for now removed features.
* ``Fixed`` for any bug fixes.
* ``Security`` in case of vulnerabilities.

0.1.0 (2021-04-27)
------------------------------
* Added core functionality: Yen's k simple shortest paths algorithm.
